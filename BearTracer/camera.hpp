//
//  camera.hpp
//  BearTracer
//
//  Created by Bjoern Siegert on 29/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef camera_hpp
#define camera_hpp

#include <stdio.h>
#include "utils/vector3.hpp"
#include "ray.hpp"

// Camera object
class Camera
{
public:
    
    // -------- CONSTRUCTOR --------- //
    Camera();
    
    // fov is top to bottom and in degrees
    Camera(const vec3& position, const vec3& look_at, const vec3 &view_up, const float& fov_vertical, const float& aspect, const float& aperture, const float& focus_distance);
    
    vec3 m_lower_left_corner;
    vec3 m_horizontal;
    vec3 m_vertical;
    vec3 m_origin;
    float m_lens_radius;
    vec3 m_u, m_v, m_w;
    
    ray get_ray(const float& u, const float& v);
};

vec3 random_in_unit_disk();

#endif /* camera_hpp */
