//
//  main.cpp
//  BearTracer is a test raytracer and used as a playground for my self.
//  This is by no means a production renderer.
//
//  Created by Bjoern on 21/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <thread>
#include <mutex>

#include "utils/vector3.hpp"
#include "objects/sphere.hpp"
#include "hitable_list.hpp"
#include "ray.hpp"
#include "camera.hpp"
#include "materials/lambertian.hpp"
#include "materials/metal.hpp"
#include "materials/dielectric.hpp"

#include <float.h>


static std::mutex barrier;

std::vector<std::string> g_frame_buffer;


// test equation to see if both versions return the same result
void test_func(const vec3 &center, float radius, const ray &r)
{
    vec3 sphere = r.origin() - center;
    float t = 4.0;
    float equa = t*t*dot(r.direction(), r.direction()) + dot(center, center) + dot(sphere, sphere) + 2*t*dot(r.direction(), center - sphere) - 2*dot(center, sphere);
    std::cout << "equation 1: " << equa << std::endl;
    float equa2 = t*t*dot(r.direction(), r.direction()) + 2*t*dot(r.direction(), center - sphere) + dot(center-sphere, center-sphere);
    std::cout << "equation 2: " << equa2 << std::endl;
}


float hit_sphere(const vec3 &center, float radius, const ray &r)
{
    // where does ray hit the sphere can be described as:
    // dot( (p(t) - C), (p(t) - C) ) = R^2
    // while C is the the center of the sphere
    // dot( (A + t*B - C), (A + t*B - C) ) = R^2
    // (a + b + c)^2 = a^2 + b^2 + c^2 + 2ab - 2ac - 2bc
    // If we apply this to our equation we get the following:
    //
    // (tB)^2 + 2tB(A - C) + A^2 - 2AC + C^2 - R^2 = 0 -> quadratic function to solve: ax^2 + bx + c = 0
    //   |            |                |
    //   a            b                c
    // We can simplify (c) further knowing that A^2 - 2AC + C^2 = (A - C)^2
    // and replace ^2 with the dot() function
    // t^2*dot(B,B) + t*2*dot(B,A-C) + dot(A-C, A-C)- R^2 = 0
    // now we can sovle that equation to t
    
    // center point of sphere
    vec3 ac = r.origin() - center; // A-C
    float a = dot(r.direction(), r.direction()); // dot(B,B)
    float b = 2.0 * dot(r.direction(), ac); // 2*dot(B, A-C)
    float c = dot(ac, ac) - radius*radius; // dot(A-C, A-C) - R*R
    float discriminant = b*b - 4*a*c;
    // if the discriminant is > 0 we have two hits with the ray on our sphere (front and back)
    // if it is == 0 we have one one hit (at the edge) and < 0 means that there is no hit at all
    
    if (discriminant < 0)
    {
        return -1.0;
    }
    else
    {
        return (-b - sqrt(discriminant)) / (2.0*a);
    }
}

vec3 ray_color(const ray &r)
{
    vec3 color(0, 0, 0);
    const vec3 sphere_position = vec3(0, 0, -1);
    float t = hit_sphere(sphere_position, 0.5, r);
    if(t > 0.0)
    {
        vec3 N = unit_vector(r.point_at_parameter(t) - sphere_position);
        // we want to shift the normal to positive values (+1) and we half the result
        // to stay in 0-1 space.
        color = 0.5*vec3(N.x() + 1., N.y() + 1., N.z() + 1.);
    }
    else
    {
        vec3 unit_direction = unit_vector(r.direction());
        t = 0.5 * (unit_direction.y() + 1.0);
        color = (1.0 - t) * vec3(1.0, 1.0, 1.0) + t * vec3(0.5, 0.7, 1.0);
    }
    return color;
}

// Color hit function
vec3 color_func(const ray &r, HitableList *world, int depth)
{
    HitRecord record;
    if(world->hit(r, 0.001, MAXFLOAT, record))
    {
        ray scattered;
        vec3 attenuation;
        if(depth < 50 && record.material_ptr->scatter(r, record, attenuation, scattered))
        {
            return attenuation * color_func(scattered, world, depth+1);
        }
        else{
            return vec3(0,0,0);
        }
    }
    else
    {
        vec3 unit_direction = unit_vector(r.direction());
        float t = 0.5*(unit_direction.y()+1.0);
        return (1.0-t)*vec3(1, 1, 1)+ t*vec3(0.5, 0.7, 1.0);
    }
}

// the colums/ pixels written form left to right
void compute_scanline(std::string &scanline, Camera *render_cam, HitableList *world, const int &row, const int &nx, const int &ny,const int &num_spp, const float &gamma)
{
    // the colums/ pixels written form left to right
    for(int colum = 0; colum < nx; colum++)
    {
        vec3 color(0, 0, 0);
        for(int s = 0; s < num_spp; s++)
        {
            float u = float(colum + drand48()) / float(nx);
            float v = float(row + drand48()) / float(ny);
            ray r = render_cam->get_ray(u, v);
            color += color_func(r, world, 0);
        }
        color /= float(num_spp);
        // doing gamma correction here.
        // Todo: need to remove when switching to exr
        int ir = int(255.99 * pow(color[0], gamma));
        int ig = int(255.99 * pow(color[1], gamma));
        int ib = int(255.99 * pow(color[2], gamma));
        scanline += std::to_string(ir) + " " + std::to_string(ig) + " " + std::to_string(ib) + "\n";
    }
}

// todo: camera and world should not be copies!
void worker_thread(Camera *render_cam, HitableList *world, const int &row, const int &nx, const int &ny, const int &num_spp, const float &gamma)
{
    std::string scanline;
    compute_scanline(scanline, render_cam, world, row, nx, ny, num_spp, gamma);
    std::lock_guard<std::mutex> block_threads_until_finished(barrier);
    g_frame_buffer[row] = scanline;
}


void ray_casting(const int &resx, const int &resy, const int &samples, const int &num_threads)
{
    const float gamma = 1/1.8;
    
    const int nx = resx;
    const int ny = resy;
    
    // samples per pixel
    const int num_spp = samples;
    
    const vec3 position = vec3(3.5, 3, 3);
    const vec3 look_at = vec3(0, 0, -1);
    const float aspect = static_cast<float>(nx) / static_cast<float>(ny);
    const float fov = 10;
    const float aperture = 5.6;
    const float focal_distance = (position - look_at).length();
    Camera render_cam(position, look_at, vec3(0, 1, 0), fov, aspect, aperture, focal_distance);

    HitableList world;
    
    // ------ MATERIALS ------- //
    std::shared_ptr<AbstractBaseMaterial> pink_mat = std::make_shared<LambertianMaterial>(LambertianMaterial(vec3(1.,.0,.3)));
    
    std::shared_ptr<AbstractBaseMaterial> grey_mat = std::make_shared<LambertianMaterial>(LambertianMaterial(vec3(.18,.18,.18)));
   
    std::shared_ptr<AbstractBaseMaterial> metal_mat = std::make_shared<MetalMaterial>(MetalMaterial(vec3(0.8, 0.8, 0.8)));
    
    std::shared_ptr<AbstractBaseMaterial> metal_fuzz_mat = std::make_shared<MetalMaterial>(MetalMaterial(vec3(0.8, 0.8, 0.8), 0.25));
    
    std::shared_ptr<AbstractBaseMaterial> glass_mat = std::make_shared<DielectricMaterial>(DielectricMaterial(1.43));
   
    // -------- GEOMETRY -------- //
    world.m_list.emplace_back(std::make_shared<Sphere>(Sphere(vec3(0,0,-1), 0.5, glass_mat)));
    
    world.m_list.emplace_back(std::make_shared<Sphere>(Sphere(vec3(0,0,-1), -0.4, glass_mat)));
    
    world.m_list.emplace_back(std::make_shared<Sphere>(Sphere(vec3(-1, 0, -1), .5, metal_fuzz_mat)));
    
    world.m_list.emplace_back(std::make_shared<Sphere>(Sphere(vec3(1, 0, -1), .5, pink_mat)));
    
//    world.m_list.emplace_back(std::make_shared<Sphere>(Sphere(vec3(-.5,0.1,-0.5), .3, pink_mat)));
    
    world.m_list.emplace_back(std::make_shared<Sphere>(Sphere(vec3(0,-100.5, -1), 100, grey_mat)));
    
    std::cout << world.m_list.size() << " Objects in scene" << std::endl;
    
    std::vector<std::thread> threads;
    
    // the rows from top to bottom
    for(int row = ny - 1; row >= 0; row--)
    {
        threads.push_back(std::thread(worker_thread, &render_cam, &world, row, nx, ny, num_spp, gamma));
    }
    
    for(auto& thread : threads)
    {
        thread.join();
    }
}

int main(int argc, const char * argv[]) {
    
    // number of concurrent threads for rendering
    const int resolution[] = {
        400,
        200
    };
    // resize our frame_buffer
    g_frame_buffer.resize(resolution[1]);
    
    const int num_threads = 4; // not used at the moment
    const int samples = 100;
    
    std::cout << "Rendering started ->" << std::endl;
    clock_t start_time = clock();
    std::thread sub_thread(ray_casting, resolution[0], resolution[1], samples, num_threads);
    sub_thread.join();
    const double render_time = (clock() - start_time) / 1000000.0;
    std::cout << "Render done in: " << render_time << "sec" << std::endl;
    
    // the output file which we write to
    std::string filename = "multithreadding" + std::to_string(samples);
    const std::string filePath = "/Users/bjoern_siegert/GitHub/beartracer/BearTracer/output/" + filename + ".ppm";
    std::ofstream outputFile(filePath);
    if(!outputFile.is_open())
    {
        std::cout << "Error opening file." << std::endl;
        return 1;
    }
    // image header with resolution and color values
    outputFile << "P3\n" << resolution[0] << " " << resolution[1] << "\n255\n";
    for(auto it = g_frame_buffer.rbegin(); it != g_frame_buffer.rend(); ++it)
    {
        outputFile << *it;
    }
    outputFile.close();
    std::cout << "File written to disk: " << filePath << std::endl;
    
    return 0;
}
