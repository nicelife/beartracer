//
//  dielectric.cpp
//  BearTracer
//
//  Created by Bjoern Siegert on 13/10/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include "dielectric.hpp"

bool DielectricMaterial::scatter(const ray &ray_input, const HitRecord &record, vec3 &attenuation, ray &ray_scattered) const
{
    vec3 outward_normal;
    vec3 reflected = reflect(unit_vector(ray_input.direction()), record.normal);
    float ni_over_nt;
    // no absorbtion
    attenuation = vec3(1.0, 1.0, 1.0);
    vec3 refracted;
    float reflect_prob;
    float cosine;
    
    // reflection
    if(dot(ray_input.direction(), record.normal) > 0)
    {
        outward_normal = - record.normal;
        ni_over_nt = m_ior;
        cosine = m_ior * dot(ray_input.direction(), record.normal) / ray_input.direction().length();
    }
    else
    {
        outward_normal = record.normal;
        ni_over_nt = 1.0 / m_ior;
        cosine = - dot(ray_input.direction(), record.normal) / ray_input.direction().length();
    }
    
//    cosine = abs(dot(ray_input.direction()/ ray_input.direction().length(), record.normal));
    
    // refraction
    if(refract(ray_input.direction(), outward_normal, ni_over_nt, refracted))
    {
        reflect_prob = schlick(cosine, m_ior);
    }
    else
    {
//        ray_scattered = ray(record.p, reflected);
        reflect_prob = 1.0;
    }
    
    if(drand48() < reflect_prob)
    {
        ray_scattered = ray(record.p, reflected);
    }
    else
    {
        ray_scattered = ray(record.p, refracted);
    }    
    return true;
}
