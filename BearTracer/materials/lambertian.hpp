//
//  lambertian.hpp
//  BearTracer
//
//  Created by Bjoern Siegert on 6/10/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef lambertian_hpp
#define lambertian_hpp

#include <stdio.h>
#include "material.hpp"

// lambert material. Very dirty
class LambertianMaterial : public AbstractBaseMaterial
{
public:
    vec3 m_albedo;
    
    LambertianMaterial(const vec3& albedo);
    
    bool scatter(const ray& ray_input,
                 const HitRecord& record,
                 vec3& attenuation,
                 ray& ray_scattered) const;
};

#endif /* lambertian_hpp */
