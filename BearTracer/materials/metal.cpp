//
//  metal.cpp
//  BearTracer
//
//  Created by Bjoern Siegert on 13/10/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include "metal.hpp"

bool MetalMaterial::scatter(const ray &ray_input, const HitRecord &record, vec3 &attenuation, ray &ray_scattered) const
{
    vec3 reflected = reflect(unit_vector(ray_input.direction()), record.normal);
    ray_scattered = ray(record.p, reflected + m_fuzz * random_unit_sphere());
    attenuation = m_albedo;
    return (dot(ray_scattered.direction(), record.normal) > 0);
}
