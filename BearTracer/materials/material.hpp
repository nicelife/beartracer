//
//  material.hpp
//  BearTracer
//
//  Created by Bjoern Siegert on 6/10/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef material_hpp
#define material_hpp

#include "../ray.hpp"
#include "../hitable.hpp"

// Base material class
class AbstractBaseMaterial
{
public:
    
    // ray scattering
    virtual bool scatter(const ray& ray_input,
                         const HitRecord& record,
                         vec3& attenuation,
                         ray& ray_scattered) const = 0;
};


// Get a random point in a unit sphere
// this is very dirty and should be changed to a proper lambert implemenation
vec3 random_unit_sphere();

// ray reflection function which calculates the reflected output direction of the ray
vec3 reflect(const vec3& ray_direction, const vec3& normal);

// ray refraction function
bool refract(const vec3& ray_direction, const vec3& normal, const float& ni_over_nt, vec3& refracted);

// schlick
float schlick(const float& cosine, const float& ref_idx);

#endif /* material_hpp */
