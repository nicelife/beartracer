//
//  material.cpp
//  BearTracer
//
//  Created by Bjoern Siegert on 13/10/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include <stdio.h>
#include "material.hpp"


vec3 random_unit_sphere()
{
    vec3 p;
    do
    {
        // random point on unit sphere
        // 2.0 to cope with negative values. 0,0,0 is the mid point of the unit sphere
        // possible values would be e.g. -1,-1, 0.5 -> p stays between -1 and 1
        p = 2.0 * vec3(drand48(), drand48(), drand48()) - vec3(1, 1, 1);
    }
    while (p.squared_length() >= 1.0);
    return p;
}

vec3 reflect(const vec3 &ray_direction, const vec3 &normal)
{
    return ray_direction - 2 * dot(ray_direction, normal) * normal;
}


bool refract(const vec3& ray_direction, const vec3& normal, const float& ni_over_nt, vec3& refracted)
{
    const vec3 unit_vec = unit_vector(ray_direction);
    float dt = dot(unit_vec, normal);
    float discriminant = 1.0 - ni_over_nt * ni_over_nt * (1 - dt*dt);
    if(discriminant > 0)
    {
        refracted = ni_over_nt * (unit_vec - normal * dt) - normal * sqrt(discriminant);
        return true;
    }
    else
        return false;
}

float schlick(const float& cosine, const float& ref_idx)
{
    float r0 = (1 - ref_idx) / (1 + ref_idx);
    r0 = r0 * r0;
    return r0 + (1 - r0) * pow((1 - cosine), 5);
}
