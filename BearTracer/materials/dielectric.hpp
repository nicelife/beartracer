//
//  dielectric.hpp
//  BearTracer
//
//  Created by Bjoern Siegert on 13/10/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef dielectric_hpp
#define dielectric_hpp

#include <stdio.h>
#include "material.hpp"

class DielectricMaterial : public AbstractBaseMaterial
{
public:
    
    float m_ior;
    
    DielectricMaterial(float ri) : m_ior(ri) {};
    
    bool scatter(const ray& ray_input,
                 const HitRecord& record,
                 vec3& attenuation,
                 ray& ray_scattered) const;
    
};

#endif /* dielectric_hpp */
