//
//  metal.hpp
//  BearTracer
//
//  Created by Bjoern Siegert on 13/10/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef metal_hpp
#define metal_hpp

#include <stdio.h>
#include "material.hpp"


class MetalMaterial : public AbstractBaseMaterial
{
public:
    
    vec3 m_albedo;
    
    // roughness of material
    float m_fuzz;
    
    MetalMaterial(const vec3& albedo, const float& fuzz=0) : m_albedo(albedo)
    {
        if(fuzz < 1) m_fuzz = fuzz; else m_fuzz = 1;
    };
    
    // scatter the incoming ray. Calculates roughness based on m_fuzz
    bool scatter(const ray& ray_input,
                 const HitRecord& record,
                 vec3& attenuation,
                 ray& ray_scattered) const;
    
};


#endif /* metal_hpp */
