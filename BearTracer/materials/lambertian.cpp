//
//  lambertian.cpp
//  BearTracer
//
//  Created by Bjoern Siegert on 6/10/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include "lambertian.hpp"

LambertianMaterial::LambertianMaterial(const vec3& albedo)
{
    m_albedo = albedo;
}

bool LambertianMaterial::scatter(const ray &ray_input, const HitRecord &record, vec3 &attenuation, ray &ray_scattered) const
{
    vec3 target = record.normal + random_unit_sphere();
    ray_scattered = ray(record.p, target);
    attenuation = m_albedo;
    return true;
}
