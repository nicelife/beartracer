//
//  sphere.cpp
//  BearTracer
//
//  Created by Bjoern Siegert on 23/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include "sphere.hpp"

bool Sphere::hit(const ray& r, float t_min, float t_max, HitRecord& record) const
{
    vec3 oc = r.origin() - center;
    float a = dot(r.direction(), r.direction());
    float b = 2*dot(oc, r.direction());
    float c = dot(oc, oc) - radius*radius;
    float discriminant = b*b - 4*a*c;
    // set material
    record.material_ptr = m_material_ptr;
    if(discriminant > 0)
    {
        float temp = (-b - sqrt(discriminant)) / (2.0*a);
        if(temp < t_max && temp > t_min)
        {
            record.t = temp;
            record.p = r.point_at_parameter(record.t);
            record.normal = (record.p - center) / radius;
            return true;
        }
        temp = (-b + sqrt(discriminant)) / (2.0*a);
        if(temp < t_max && temp > t_min)
        {
            record.t = temp;
            record.p = r.point_at_parameter(record.t);
            record.normal = (record.p - center) / radius;
            return true;
        }
    }
    return false;
}

