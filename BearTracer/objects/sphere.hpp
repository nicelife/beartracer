//
//  sphere.hpp
//  BearTracer
//
//  Created by Bjoern Siegert on 23/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef sphere_hpp
#define sphere_hpp

#include <stdio.h>
#include "../hitable.hpp"


// A test sphere for our renderer
class Sphere : public AbstractHitable
{
public:
    Sphere() {}
    Sphere(vec3 cen, float r, std::shared_ptr<AbstractBaseMaterial> material) : center(cen), radius(r), m_material_ptr(material) {};
    
    // center point of sphere
    vec3 center;
    
    // radius of sphere
    float radius;
    
    // pointer to material
    std::shared_ptr<AbstractBaseMaterial> m_material_ptr;
    
    // hit function
    virtual bool  hit(const ray& r,
                      float t_min,
                      float t_max,
                      HitRecord& record) const;
};

#endif /* sphere_hpp */
