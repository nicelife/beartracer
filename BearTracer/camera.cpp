//
//  camera.cpp
//  BearTracer
//
//  Created by Bjoern Siegert on 29/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include "camera.hpp"
#include <iostream>

Camera::Camera()
{
    m_lower_left_corner = vec3(-2., -1., -1.);
    m_horizontal = vec3(4., 0., 0.);
    m_vertical = vec3(0., 2., 0.);
    m_origin = vec3(0., 0., 0.);
}

Camera::Camera(const vec3& position, const vec3& look_at, const vec3 &view_up, const float& fov_vertical, const float& aspect, const float& aperture=0, const float& focus_distance=1.0)
{
    if(aperture == 0)
    {
        m_lens_radius = 0;
    }
    else
    {
        m_lens_radius = (1.0/aperture) / 2;
    }
    const float theta = fov_vertical * M_PI/180;
    const float half_height = tan(theta/2);
    const float half_width = aspect * half_height;
    m_origin = position;
    m_w = unit_vector(position - look_at);
    m_u = unit_vector(cross(view_up, m_w));
    m_v = cross(m_w, m_u);
    m_horizontal = 2 * half_width * m_u * focus_distance;
    m_vertical = 2 * half_height * m_v * focus_distance;
    m_lower_left_corner = m_origin - m_horizontal/2 - m_vertical/2 - m_w * focus_distance;
    
    std::cout << "Camera: " << std::endl;
    std::cout << "aspect: " << aspect << std::endl;
    std::cout << "u: " << m_u << std::endl;
    std::cout << "v: " << m_v << std::endl;
    std::cout << "w: " << m_w << std::endl;
    std::cout << "lower_left_corner: " << m_lower_left_corner << std::endl;
    std::cout << "m_horizontal: " << m_horizontal << std::endl;
    std::cout << "m_vertical: " << m_vertical << std::endl;
}

ray Camera::get_ray(const float &u, const float &v)
{
    const vec3 rd = m_lens_radius * random_in_unit_disk();
    const vec3 offset = m_u * rd.x() + m_v * rd.y();
    return ray(m_origin + offset, (m_lower_left_corner + u*m_horizontal + v*m_vertical) - m_origin - offset);
}

vec3 random_in_unit_disk()
{
    vec3 p;
    do{
        p = 2.0 * vec3(drand48(), drand48(), 0) - vec3(1,1,0);
    } while (dot(p, p) >= 1.0);
    return p;
}
