//
//  vector3.hpp
//  BearTracer
//
//  Created by Bjoern on 21/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef vector3_hpp
#define vector3_hpp

#include <stdio.h>
#include <math.h>
#include <iostream>

class vec3
{
    
public:
    
    float m_data[3];
    
    // constructor
    vec3() {}
    
    vec3(float e0, float e1, float e2);
    
    // member functions
    
    // coordinates
    inline float x() const { return m_data[0]; }
    inline float y() const { return m_data[1]; }
    inline float z() const { return m_data[2]; }
    
    // color values
    inline float r() const { return m_data[0]; }
    inline float g() const { return m_data[1]; }
    inline float b() const { return m_data[2]; }
    
    //
    inline const vec3& operator+() const { return *this; }
    
    inline vec3 operator-() const { return vec3(-m_data[0], -m_data[1], -m_data[2]); }
    
    inline float operator[](int i) const { return m_data[i]; }
    
    inline float& operator[](int i) { return m_data[i]; }
    
    // operators
    vec3& operator+=(const vec3 &v2);
    vec3& operator-=(const vec3 &v2);
    vec3& operator*=(const vec3 &v2);
    vec3& operator/=(const vec3 &v2);
    vec3& operator*=(const float mutiplier);
    vec3& operator/=(const float divisor);
    
    // length of vector
    float length() const;
    
    //
    float squared_length() const;
    
    //
    void make_unit_vector();
};

// ----------- operator overloading ------------ //
inline vec3 operator+(const vec3 &v1, const vec3 &v2)
{
    return vec3(
                v1.m_data[0] + v2.m_data[0],
                v1.m_data[1] + v2.m_data[1],
                v1.m_data[2] + v2.m_data[2]
                );
}

inline vec3 operator-(const vec3 &v1, const vec3 &v2)
{
    return vec3(
                v1.m_data[0] - v2.m_data[0],
                v1.m_data[1] - v2.m_data[1],
                v1.m_data[2] - v2.m_data[2]
                );
}

inline vec3 operator/(const vec3 &v1, const vec3 &v2)
{
    return vec3(
                v1.m_data[0] / v2.m_data[0],
                v1.m_data[1] / v2.m_data[1],
                v1.m_data[2] / v2.m_data[2]
                );
}

inline vec3 operator/(const vec3 &v1, float divisor)
{
    return vec3(
                v1.m_data[0] / divisor,
                v1.m_data[1] / divisor,
                v1.m_data[2] / divisor
                );
}

inline vec3 operator*(const vec3 &v1, const vec3 &v2)
{
    return vec3(
                v1.m_data[0] * v2.m_data[0],
                v1.m_data[1] * v2.m_data[1],
                v1.m_data[2] * v2.m_data[2]
                );
}

inline vec3 operator*(float mutiplier, const vec3 &v1)
{
    return vec3(
                mutiplier * v1.m_data[0],
                mutiplier * v1.m_data[1],
                mutiplier * v1.m_data[2]
                );
}

inline vec3 operator*(const vec3 &v1, float mutiplier)
{
    return vec3(
                v1.m_data[0] * mutiplier,
                v1.m_data[1] * mutiplier,
                v1.m_data[2] * mutiplier
                );
}

inline float dot(const vec3 &v1, const vec3 &v2)
{
    return v1.m_data[0] * v2.m_data[0] + v1.m_data[1] * v2.m_data[1] + v1.m_data[2] * v2.m_data[2];
}

inline vec3 cross(const vec3 &v1, const vec3 &v2)
{
    return vec3(
                (v1.m_data[1] * v2.m_data[2] - v1.m_data[2] * v2.m_data[1]),
                -(v1.m_data[0] * v2.m_data[2] - v1.m_data[2] * v2.m_data[0]),
                (v1.m_data[0] * v2.m_data[1] - v1.m_data[1] * v2.m_data[0])
                );
}

// iostream operators
inline std::istream& operator>>(std::istream &is, vec3 &v)
{
    is >> v.m_data[0] >> v.m_data[1] >> v.m_data[2];
    return is;
}

inline std::ostream& operator<<(std::ostream &os, vec3 &v)
{
    os << "[" << v.m_data[0] << ", " << v.m_data[1] << ", " << v.m_data[2] << "]";
    return os;
}

inline vec3 unit_vector(vec3 v)
{
    return v / v.length();
}

#endif /* vector3_hpp */
