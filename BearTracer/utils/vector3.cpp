//
//  vector3.cpp
//  BearTracer
//
//  Created by Bjoern Siegert on 21/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include "vector3.hpp"
#include <iostream>

vec3::vec3(float e0, float e1, float e2)
{
    m_data[0] = e0;
    m_data[1] = e1;
    m_data[2] = e2;
}

vec3& vec3::operator+=(const vec3 &v2)
{
    m_data[0] += v2.m_data[0];
    m_data[1] += v2.m_data[1];
    m_data[2] += v2.m_data[2];
    return *this;
}

vec3& vec3::operator-=(const vec3 &v2)
{
    m_data[0] -= v2.m_data[0];
    m_data[1] -= v2.m_data[1];
    m_data[2] -= v2.m_data[2];
    return *this;
}

vec3& vec3::operator*=(const vec3 &v2)
{
    m_data[0] *= v2.m_data[0];
    m_data[1] *= v2.m_data[1];
    m_data[2] *= v2.m_data[2];
    return *this;
}

vec3& vec3::operator/=(const vec3 &v2)
{
    m_data[0] /= v2.m_data[0];
    m_data[1] /= v2.m_data[1];
    m_data[2] /= v2.m_data[2];
    return *this;
}

vec3& vec3::operator*=(const float t)
{
    m_data[0] *= t;
    m_data[1] *= t;
    m_data[2] *= t;
    return *this;
}

vec3& vec3::operator/=(const float t)
{
    const float k = 1.0/t;
    m_data[0] *= k;
    m_data[1] *= k;
    m_data[2] *= k;
    return *this;
}

float vec3::length() const
{
    return sqrt((m_data[0] * m_data[0]) +
                (m_data[1] * m_data[1]) +
                (m_data[2] * m_data[2]) );
}

float vec3::squared_length() const
{
    return (m_data[0] * m_data[0]) + (m_data[1] * m_data[1]) + (m_data[2] * m_data[2]);
}

void vec3::make_unit_vector()
{
    float k = 1.0 / sqrt(m_data[0]*m_data[0] * m_data[1]*m_data[1] + m_data[2]*m_data[2]);
    m_data[0] *= k;
    m_data[1] *= k;
    m_data[2] *= k;
}
