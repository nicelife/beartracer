//
//  ray.cpp
//  BearTracer
//
//  Created by Bjoern Siegert on 22/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include "ray.hpp"

// Ray
ray::ray(const vec3 &a, const vec3 &b)
{
    A = a;
    B = b;
}

vec3 ray::origin() const
{
    return A;
}

vec3 ray::direction() const
{
    return B;
}

vec3 ray::point_at_parameter(float t) const
{
    return A + t * B;
}
