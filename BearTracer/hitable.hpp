//
//  hitable.hpp
//  BearTracer
//
//  Created by Bjoern Siegert on 23/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef hitable_hpp
#define hitable_hpp

#include <stdio.h>
#include "ray.hpp"

class AbstractBaseMaterial;

// Hit data storage object
struct HitRecord
{
    float t;
    
    vec3 p;
    
    vec3 normal;
    
    // used material on hitable
    std::shared_ptr<AbstractBaseMaterial> material_ptr;
};

// Hit object for raytracer
class AbstractHitable
{
    
public:
    
    // hit function which stores the hit data
    virtual bool hit(const ray& r,
                     float t_min,
                     float t_max,
                     HitRecord& record) const = 0;
};


#endif /* hitable_hpp */
