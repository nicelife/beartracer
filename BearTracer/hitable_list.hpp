//
//  hitable_list.hpp
//  BearTracer
//
//  Created by Bjoern Siegert on 26/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef hitable_list_hpp
#define hitable_list_hpp

#include <stdio.h>
#include <vector>
#include <memory>
#include "hitable.hpp"


class HitableList : public AbstractHitable
{
public:
    
    HitableList(){}
    
    bool hit(const ray &r, float t_min, float t_max, HitRecord
         &rec) const;
    
    // ----------------- MEMBERS ---------------------- //
    
    // list of active objects in the scene
    std::vector<std::shared_ptr<AbstractHitable>> m_list;
};

#endif /* hitable_list_hpp */
