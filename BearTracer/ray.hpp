//
//  ray.hpp
//  BearTracer
//
//  Created by Bjoern Siegert on 22/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#ifndef ray_hpp
#define ray_hpp

#include <stdio.h>

#include "utils/vector3.hpp"


class ray
{
public:
    
    vec3 A;
    vec3 B;
    
    ray() {}
    
    /*
     * /param origin of ray
     * /param direction of ray
     */
    ray(const vec3 &a, const vec3 &b);
    
    vec3 origin() const;
    vec3 direction() const;
    vec3 point_at_parameter(float t) const;
};


#endif /* ray_hpp */
