//
//  hitable_list.cpp
//  BearTracer
//
//  Created by Bjoern Siegert on 26/09/18.
//  Copyright © 2018 Bjoern. All rights reserved.
//

#include "hitable_list.hpp"

bool HitableList::hit(const ray &r, float t_min, float t_max, HitRecord &rec) const
{
    HitRecord temp_rec;
    bool hit_anything = false;
    double closest_so_far = t_max;
    for(const auto &item : m_list)
    {
        if (item->hit(r, t_min, closest_so_far, temp_rec))
        {
            hit_anything = true;
            closest_so_far = temp_rec.t;
            rec = temp_rec;
        }
    }
    return hit_anything;
}
